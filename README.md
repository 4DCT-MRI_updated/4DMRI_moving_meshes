# 4DMRI moving meshes

**Moving lung and ribcage meshes extracted from 4DMRIs**

----------------------------------------------------------------------------------------------------

Please cite the following publications when using the data:

Jenny, T., Duetschler, A., Weber, D.C., Lomax, A.J. and Zhang, Y. (2023), *Technical Note: Towards more realistic 4DCT(MRI) numerical lung phantoms*. Med. Phys.. [https://doi.org/10.1002/mp.16451](https://doi.org/10.1002/mp.16451)

Duetschler, A., Bauman, G., Bieri, O., Cattin, P.C., Ehrbar, S., Engin-Deniz, G., Giger, A., Josipovic, M., Jud, C., Krieger, M., Nguyen, D., Persson, G.F., Salomir, R., Weber, D.C., Lomax, A.J. and Zhang, Y. (2022), *Synthetic 4DCT(MRI) lung phantom generation for 4D radiotherapy and image guidance investigations*. Med. Phys.. [https://doi.org/10.1002/mp.15591](https://doi.org/10.1002/mp.15591)

Duetschler, A., Jenny, T., Weber, D.C., Lomax, A.J. and Zhang, Y. (2022). 4DMRI moving meshes. Zenodo. [doi:10.5281/zenodo.7052607](https://doi.org/10.5281/zenodo.7052607)

In case of questions, please contact Alisha Dütschler (alisha.duetschler@psi.ch).

----------------------------------------------------------------------------------------------------

The moving lung and ribcage meshes can for example be used to generate synthetic 4DCT(MRI)s. The code for generating 4DCT(MRI)s using a reference CT and the moving mesh data can be found [here](https://gitlab.psi.ch/4DCT-MRI_updated/4DCT-MRI). 

----------------------------------------------------------------------------------------------------
**Clone repository**

You can clone the repository from Gitlab. In the terminal navigate to the desired location and clone the repository

    git clone https://gitlab.psi.ch/4DCT-MRI_internal/v1/4DMRI_moving_lung_meshes
    
----------------------------------------------------------------------------------------------------
### 4DMRI acquisition and mesh extraction

4DMRIs have been acquired for two volunteers over multiple breathing cycles in free berathing. The 4DMRIs were acquired and reconstructed with a navigator-based method [1]. Alternatingly, a navigator slice and slices covering the whole body were obtained. The navigator slice was then used retrospectively to match the slices with similar lung states. Afterwards, the corresponding slices were stacked together to obtain a 3DMRI for a specific time-step. The 4DMRIs of two volunteers are referred to as MRI1 and MRI2. From each reconstructed 4DMRI multiple breathing cycles were extracted (MR1: 47, MRI2: 60) using and end exahle (EE) reference phase. Both 4DMRIs have an effective temporal resolution of 2.15 Hz. Examples of EE and EI phases of the two MRIs are presented in Figure 1.

![MRI](Figures/MRI.png)
***Figure 1.*** EE and EI states MRI1 ((a) & (b)) and MRI2 ((c) & (d)). The diaphragm is indicated with a white line.

Both lung halves and the ribcage were manually segmented on the chosen EE reference states and converted to surface meshes using [vtk](https://vtk.org/)[2]. A regular grid was then inserted inside the surface meshes. The meshes are then animated using the deformation vectors extracted at the mesh point locations.

### 4DMRI moving mesh data 

Folder structure: 
The reference phase lung and ribcage masks (lungs_left.mha, lungs_right.mha and ribcage.mha) can be found in the structures folder for each MRI. These masks were then smoothed and downsampled (lungs_left_smooth.mha, lungs_right_smooth.mha and ribcage_smooth.mha) and the resulting masks were used to obtain the reference surface meshes (lungs_left.vtk, lungs_right.vtk and ribcage.vtk). 
The MovingMRmesh folder again contains the reference surface meshes (lungs_left_surface.vkt, lungs_right_surface.vkt and ribcage_surface.vtk), a regular grid of points inside each mesh (lungs_left_grid.vkt, lungs_right_grid.vkt and ribcage_grid.vtk) and both combined (lungs_left_surface_and_grid.vtk, lungs_right and ribcage_surface_and_grid.vtk). The subfolders Left and Right contain the moving lung MR meshes for each 4DMRI state, while the Ribcage subfolder contains the moving ribcage meshes (state_000X.vkt). 

The motion patterns are visualized in Figures 2 and 3.

<p align="middle">
  <img src="Figures/MRI1_motion.png" width="48%" />
  <img src="Figures/MRI2_motion.png" width="48%" /> 
</p>

***Figure 2.*** Visualization of mesh motion. For each MRI a point in the dome of each lung half was selected and the superior-inferior (SI) motion of each lung half is plotted over time.

<p align="middle">
  <img src="Figures/amplitudes.png" width="48%" />
  <img src="Figures/periods.png" width="48%" /> 
</p>

***Figure 3.*** Boxplots of mesh motion amplitudes and periods over all breathing cycles.


## References

[1] von Siebenthal M, Székely G, Gamper U, Boesiger P, Lomax A, Cattin P. *4D MR imaging of respiratory organ motion and its variability.* Phys Med Biol.; 2007; 52(6):1547-1564. doi:10.1088/0031-9155/52/6/001

[2] Schroeder W, Martin K, Lorensen B. *The Visualization Toolkit.* 4th ed. Kitaware; 2006
